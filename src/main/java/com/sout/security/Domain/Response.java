package com.sout.security.Domain;

import java.io.Serializable;

public class Response implements Serializable {

    private static final long serialVersionUID = 23456789765L;

    private String message;

    public Response(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
