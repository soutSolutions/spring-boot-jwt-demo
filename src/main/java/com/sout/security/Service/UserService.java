package com.sout.security.Service;

import com.sout.security.Model.User;

import java.util.List;

public interface UserService {
    public User save(User user);

    List<User> findAll();

    User getUserByEmail(String email);
}
