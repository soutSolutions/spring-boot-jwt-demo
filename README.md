1) Run the SecurityApplication.java

2) hit http://localhost:8080/getusers with following request body.
    > {
      "firstName" : "kolis",
      "lastName" : "kolis",
      "email" : "koli@sokol.coms",
      "password" : "654321s",
      "enabled" : true,
      "role" : "ADMIN",
      "phoneNumber" : "1234565789",
      "authorities" : [{}]
      }
  
3) Now login : http://localhost:8080/login
    > {
      "email" : "koli@sokol.coms",
      "password" : "654321s"
      }

4) Get User : http://localhost:8080/getusers. Set Authorization header with token

    > Authorization:Bearer *TOKEN_VALUE which you receive in response of step 3.(response param name : token)*
    
    
    
     